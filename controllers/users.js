const User = require('../models/users');

exports.getUsers = async (req, res, next) => {
    const results = await User.find({});
    return results;
}